//Environment Variables object
EnvVars = function() {
	//map of variables
	this.variables = {};
	this.IS_ON_TOUCH_DEVICE = null;
	this.IS_ON_MOBILE_DEVICE = null;
	this.IS_ON_ANDROID_DEVICE = null;
	this.IS_ON_IOS_DEVICE = null;
	this.IOS_VERSION = null;
	this.IS_ON_IE8 = null;
	this.IS_ON_IE10 = null;
	this.IS_ON_SMARTPHONE_DEVICE = null;
	//Local storage variables keys
	this.SOCIAL_SPACE_ELEMENT_CHANGE_TITLE = '_SOCIAL_SPACE_ELEMENT_CHANGE_TITLE';
	this.USER_PROFILE_LANGUAGE = '_USER_PROFILE_LANGUAGE'; //Al logout viene cancellata dalla localStorage del browser (su logout.jsp).
};


//set all variables.
EnvVars.prototype.setVariables = function(variables) {
	this.variables = variables;
};

//get all variables.
EnvVars.prototype.getVariables = function() {
	return this.variables;
};

//add a variable value to the map.
EnvVars.prototype.addLabel = function(key, value) {
	this.variables[key] = value;
};

//get the variable value by the key.
EnvVars.prototype.getValue = function(key) {
	if (typeof this.variables[key] == 'undefined') {
		return 'VARIABLE_NOT_FOUND';
	}
	else
		return this.variables[key];
};

EnvVars.prototype.getContextPath = function() {
	return this.getValue('contextPath');
};

EnvVars.prototype.getShLibToken = function() {
	return this.getValue('shLibToken');
};

/* ******************************************************************************************
 * Load current user info
 ****************************************************************************************** */
EnvVars.prototype.loadCurrentUserInfo = function() {
	$(document).ready(function(){
		var ctxObject = buildNewCtxObject(),
			currentUserID = ENV_VARS.getValue('currentUserID'),
			currentUserInfo = ENV_VARS.getCurrentUserInfo();
		if (currentUserInfo != null || currentUserID == '-1')
			return;
		addDataToCtx(ctxObject, 'userId', currentUserID);
		addDataToCtx(ctxObject, 'scope', 'DEFAULT');
		var rasterCtx = getRasterCtxContainer(ctxObject);
		var statusResult = null;
		$.ajax({
			data: {revent:'GET_DATA',a0:'userInfo',a1:rasterCtx},
			success: function(response, textStatus, xhr) {
				statusResult = JSON.parse(response);
				ENV_VARS.addLabel('currentUserInfo', JSON.parse(response).data.userInfo);
				rootPage.DECISYON_CORE.setUserInfo(ENV_VARS.getCurrentUserInfo());
			},
			error: function(response, textStatus, xhr) {
				logToConsole('getCurrentUserInfo AJAX call Error:' + response.responseText);
			}
		});
	});
}

//load all variables value to the map from a json string.
EnvVars.prototype.loadVariables = function(jsonStr) {
	var objVariables = JSON.parse(jsonStr);
	this.setVariables(objVariables);
	this.loadCurrentUserInfo();
};


EnvVars.prototype.isInDevelopment = function(){
	var result = this.getValue('isInDevelopment')=="true";
	return result;
};

EnvVars.prototype.isDebugModeEnabled = function(){
	var result = (ENV_VARS.isInDevelopmentOrTesting() || window.debugMode == true);
	return result;
};

EnvVars.prototype.isOnIe8 = function(){
	if (this.IS_ON_IE8 != null) {
		return this.IS_ON_IE8;
	}

	var result = false;
	if ($.browser.msie && parseInt($.browser.version) < 9) {
		result = true;
	}
	this.IS_ON_IE8 = result;
	return result;

};

EnvVars.prototype.isOnIe10 = function(){
	if (this.IS_ON_IE10 != null) {
		return this.IS_ON_IE10;
	}

	var result = false;
	if ($.browser.msie && parseInt($.browser.version) == 10 ) {
		result = true;
	}
	this.IS_ON_IE10 = result;
	return result;

};

EnvVars.prototype.isOnFirefox = function(){
	if ($.browser.mozilla && !$.browser.msie) {
		return true;
	}

	return false;
};



EnvVars.prototype.isInTesting = function(){
	var result = this.getValue('isInTesting')=="true";
	return result;
};
EnvVars.prototype.isInDevelopmentOrTesting = function(){
	var result = this.getValue('isInDevelopmentOrTesting')=="true";
	return result;
};

EnvVars.prototype.needSlimScroll = function() {
	var result = !(ENV_VARS.isOnMobileDevice() || (ENV_VARS.isOnMobileDevice() && $.browser.msie));
	return result;
};

EnvVars.prototype.isOnTouchDevice = function(){
	if (this.IS_ON_TOUCH_DEVICE != null) {
		return this.IS_ON_TOUCH_DEVICE;
	}

	var onTouch = false;
	try {
		document.createEvent("TouchEvent");
		onTouch = true;
	} catch (e) {
		onTouch = /Touch/i.test(navigator.userAgent);
	}

	var result = onTouch && (!!('ontouchstart' in window) || !!('onmsgesturechange' in window));

	this.IS_ON_TOUCH_DEVICE = result;

	return result;
};

EnvVars.prototype.isOnMobileDevice = function(){
	if (this.IS_ON_MOBILE_DEVICE != null) {
		return this.IS_ON_MOBILE_DEVICE;
	}

	var result = /Android|IEMobile|webOS|iPhone|iPad|iPod|ARM|Touch|BlackBerry/i.test(navigator.userAgent);

	this.IS_ON_MOBILE_DEVICE = result;

	return result;
};

EnvVars.prototype.isOnIosDevice = function(){
	if (this.IS_ON_IOS_DEVICE != null) {
		return this.IS_ON_IOS_DEVICE;
	}

	var result = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );

	this.IS_ON_IOS_DEVICE = result;

	return result;
};

EnvVars.prototype.getIosVersion = function(){
	if (this.IOS_VERSION != null) {
		return this.IOS_VERSION;
	}

	if (ENV_VARS.isOnIosDevice()) {
	    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
	    //this.IOS_VERSION = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
	    this.IOS_VERSION = parseInt(v[1], 10);
	  }

	return this.IOS_VERSION;
};

EnvVars.prototype.isOnAndroidDevice = function(){
	if (this.IS_ON_ANDROID_DEVICE != null) {
		return this.IS_ON_ANDROID_DEVICE;
	}

	var result = /Android/i.test(navigator.userAgent);

	this.IS_ON_ANDROID_DEVICE = result;

	return result;
};

EnvVars.prototype.isOnLowPerfDevice = function(){
	/* Device detection. */
	// For use within normal web clients
	var isiPad1 = navigator.userAgent.match(/iPad|ARM/i) != null;

	// For use within iPad developer UIWebView
	// Thanks to Andrew Hedges!
	var ua = navigator.userAgent;
	var isiPad2 = /iPad|ARM/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
	/* Fine Device detection. */

	var result = isiPad1 | isiPad2;
	return result;
};

/**
 * This method return if user used smartphone device
 * Inspired to http://detectmobilebrowsers.com/
 * Iphone : true
 * Ideos  : true
 * Ipad   : false
 * LG     : false
 * Nexsus : false
 * Surface: false
 * @returns {Boolean}
 */
EnvVars.prototype.isOnSmartphoneDevice = function(){

	if (this.IS_ON_SMARTPHONE_DEVICE != null) {
		return this.IS_ON_SMARTPHONE_DEVICE;
	}

	var check = false;

	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);

	return (this.IS_ON_SMARTPHONE_DEVICE = check);
};

//get the variable value by the key.
EnvVars.prototype.getCurrentUserInfo = function() {
	var currentUserInfo = this.getValue('currentUserInfo');
	if (currentUserInfo == 'VARIABLE_NOT_FOUND'){
		return null;
	}else{
		return currentUserInfo;
	}
};

var ENV_VARS=null;

// get EnvVars instance from parent window
if(rootPage && rootPage.ENV_VARS != null){
	ENV_VARS = rootPage.ENV_VARS;

}else{

	//instantiate the EnvVars object.
	ENV_VARS = new EnvVars();

	//get the evList provided as parameter into the script tag
	//var evList = document.getElementById('SCRIPT_EV_ID').getAttribute('evList');
	var evList = {
	  "contextPath": "",
	  "isInDevelopment": "true",
	  "isInTesting": "false",
	  "isInDevelopmentOrTesting": "true",
	  "cssToken": "",
	  "jsToken": "",
	  "shLibToken": "/resources/shared",
	  "templatesLoadPath": "",
	  "imgToken": "",
	  "imageResourceToken": "",
	  "callIntervalDefault": "1500",
	  "browserTimeout": "30000",
	  "browserRestartOnErr": "15000",
	  "browserSyncMinInterval": "500",
	  "noJsContent": "<NOTHING>",
	  "autoCloseDshOrganizer": "false",
	  "showWebClosureConfirm": "1",
	  "showUraLockedWarning": "1",
	  "currentUserID": "<NOTHING>", //Da vedere
	  "stayConnectedEnabled": "0",
	  "sessionID": "<NOTHING>", //Da vedere
	  "LandingPagePreselection": "2"
	};

	// load variables from scritp tag
	ENV_VARS.loadVariables(JSON.stringify(evList));
}


EnvVars.prototype.getSessionId = function(){
	var sessionId = this.getValue('sessionID');
	if (sessionId == 'VARIABLE_NOT_FOUND'){
		return null;
	}else{
		return sessionId;
	}

}

EnvVars.prototype.getKeyLSSocialSpaceElementChangeTitle = function(){
	return this.getValue('currentUserID') + this.SOCIAL_SPACE_ELEMENT_CHANGE_TITLE;
}

EnvVars.prototype.getKeyLSUserProfileLanguage = function(){
	return this.getSessionId() + this.USER_PROFILE_LANGUAGE;
}

/*######################## GESTIONE LOCAL STORAGE ########################*/
/*Controlla che il browser supporti la localStorage*/
function checkLocalStorageSupport() {
	try {
		return 'localStorage' in window && window['localStorage'] !== null ;
	} catch(e) {
		return false ;
	}
}


/*Ritorna il valore di una variabile nella local storage.
 * @key nome della variabile.
 * */
 EnvVars.prototype.getItemFromLocalStorage = function(key){
	if(checkLocalStorageSupport()) {
		return localStorage.getItem(key);
	}
	return null;
}

 /*Assegna un valore ad una variabile nella local storage.
  * @key nome della variabile.
  * @value valore da assegnare alla variabile.
  * */
EnvVars.prototype.setItemIntoLocalStorage = function(key, value){
	try {
		if(checkLocalStorageSupport()) {
			localStorage.setItem(key, value);
		}
		else{
			logToConsole('Local Storage not available.');
		}
	} catch(e) {
		logToConsole('Local Storage can be disabled or full.');
	}
}

/*Rimuove un item nella local storage.
 * @key nome della variabile.
 * */
EnvVars.prototype.removeItemFromLocalStorage = function(key){
	if(checkLocalStorageSupport()) {
		localStorage.removeItem(key);
	}
	else{
		logToConsole('Local Storage not available.');
	}
}

/*
 * Ritorna l'url completo a partire dall'oggetto location
 */
EnvVars.prototype.getCompleteHostUrl = function() {
	var protocol 	= location.protocol;
	var hostname 	= location.hostname;
	var port 		= location.port;
	var hostUrl 	= protocol+'//'+hostname + (port ? ':' + port : '');

	return hostUrl;
};
