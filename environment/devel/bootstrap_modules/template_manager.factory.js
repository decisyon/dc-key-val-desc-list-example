(function() {
	
	'use strict';
	
	function templateInterceptor($q, $rootScope, dcyEnvProvider, dcyUtilsFactory) {
		var isTemplateManagement = function(url){
				return (dcyUtilsFactory.isValidString(url) ? !!url.match(/templates/g) : false);
			};
		
		return {
			responseError: function(response) {
				if (angular.isObject(response.config) && isTemplateManagement(response.config.url)) {
					logToConsole('######## ERROR INTERCEPTOR FOR TEMPLATE REQUEST #### : ', response.config.url);
					response.data = '';
					
					$rootScope.$broadcast('dcy.tmplMng.errorRenderer', {url : response.config.url});
				}
				
				return $q.reject(response);
			}
		};
	}
	
	templateInterceptor.$inject = ['$q', '$rootScope', 'dcyEnvProvider', 'dcyUtilsFactory'];
	angular.module('dcyApp.factories').factory('templateInterceptor', templateInterceptor);
	
}());
