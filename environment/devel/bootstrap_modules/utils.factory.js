(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name dcyApp.UtilsFactory
   *
   * @requires $log
   * @requires dcyUtilsFactory
   *
   * @description
   * Manage how a content is opened
   *
   *
   *
   * @example
   * ```js
   * function dpFactoryFn(dcyUtilsFactory){
   *   dcyUtilsFactory.fnToCall(params);
   * }
   * myFn.$inject = ['dcyUtilsFactory'];
   *
   * angular.module('dcyApp.factories')
   *     .factory('dpFactory', dpFactoryFn);
   *
   * ```
   */

  function UtilsFactory($injector, $log, dcyConstants, $templateCache, $timeout, $window) {
    /**
     * @ngdoc Method
     * @name isValidObject
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Check if object is empty
     *
     * @param {object} obj object.
     * @returns {boolean} true if object is empty or false if object is not empty, true if obj is not valid object.
     */
	  function isValidObject(obj) {
		  var i = 0;
		  if (!angular.isObject(obj)) {
			  return false;
		  }

		  for (i in obj) {
			  return true;
		  }

		  return false;
	  }
    
    /**
     * @ngdoc Method
     * @name isValidObject
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Check if object is empty
     *
     * @param {object} obj object.
     * @returns {boolean} true if object is empty or false if object is not empty, true if obj is not valid object.
     */
    function isValidArray(arr) {
    	if (angular.isArray(arr) && arr.length) {
    		return true;
    	}
    	
    	return false;
    }

    /**
     * @ngdoc Method
     * @name isValidString
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Check if value string is valid
     *
     * @param {string} value string.
     * @returns {boolean} true if value string is a valid string and not empty string
     */
    function isValidString(value) {
      return angular.isString(value) && value !== '';
    }

    /**
     * @ngdoc Method
     * @name isHtmlElement
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Check if value string is valid
     *
     * @param {html element} element.
     * @returns {boolean} true if value element is a valid html element
     */
    function isHtmlElement(element){
    	return (
    			typeof HTMLElement === 'object' ? element instanceof HTMLElement : 
    				element && typeof element === 'object' && element !== null && element.nodeType === 1 && typeof element.nodeName==='string'
    	);
    }
    
    /**
     * @ngdoc Method
     * @name findElement
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Search element in element
     *
     * @param {query} search query.
     * @param {element} element into search.
     *
     * @returns {element} .
     */
    function findElement(query, element) {
      return angular.element((element || document).querySelectorAll(query));
    }

    /**
     * @ngdoc Method
     * @name replaceAll
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Replace strA with strB contained in text param
     *
     * @param {String} text by replaced.
     * @param {String} strA by replaced.
     * @param {String} strB by inserted.
     *
     * @returns {String} replaced string.
     */
    function replaceAll(text, strA, strB) {
      return isValidString(text) ? text.replace(new RegExp(strA, 'g'), strB) : text;
    }

    /**
     * @ngdoc Method
     * @name addEventListener
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Apply Bind event on element with handler passed
     *
     * @param {element} is a element.
     * @param {string} type event.
     * @param {function} handler callback.
     *
     * @returns {function} handler param passed.
     */
    function addEventListener(element, type, handler) {
      element.addEventListener(type, handler, false);
      return handler;
    }

    /**
     * @ngdoc Method
     * @name removeEventListener
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Remove Bind event on element with handler passed
     *
     * @param {element} is element.
     * @param {string} type event.
     * @param {function} handler callback.
     *
     * @returns {boolean} true.
     */
    function removeEventListener(element, type, handler) {
      element.removeEventListener(type, handler, false);
      return true;
    }

    /**
     * @ngdoc Method
     * @name getPropertyStyleByElement
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Get property style from element
     *
     * @param {element} is a element.
     * @param {string} style property.
     *
     * @returns {string} style property .
     */
    function getPropertyStyleByElement(element, styleProp) {
      var getValuePropertyStyle = null;

      if (window.getComputedStyle) {
        getValuePropertyStyle = document.defaultView.getComputedStyle(element, null).getPropertyValue(styleProp);
      } else if (element.currentStyle) {
        getValuePropertyStyle = element.currentStyle[styleProp];
      }

      return getValuePropertyStyle;
    }

    /**
     * @ngdoc Method
     * @name getZindexByElement
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Get max zindex applied in document by element
     *
     * @param {element} is a element.
     *
     * @returns {string} max zindex calculate.
     */
    function getZindexByElement(element) {
      var zIndexCurrentElement = 0,
          zIndex = 0;

      do {
        zIndexCurrentElement = parseInt(getPropertyStyleByElement(element, 'z-index'));
        if (zIndexCurrentElement > zIndex) {
          zIndex = zIndexCurrentElement;
        }
      } while ((element = element.offsetParent));

      return zIndex;
    }


    /**
     * @ngdoc Method
     * @name isUndefinedOrNull
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Check if value is undefined or null
     *
     * @param {generic} value by checked.
     *
     * @returns {boolean} true if undefined or null.
     */
    function isUndefinedOrNull(value) {
      return angular.isUndefined(value) || value === null;
    }

    /**
     * @ngdoc Method
     * @name adapterForNewCtx
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Convert old ctx in new ctx format
     *
     * @param {array} ctx by converted.
     *
     * @returns {object} ctx in new format.
     */
    function adapterForNewCtx(ctx) {
      var ctxAdapter = ctx;

      //Old ctx -> trasform in new ctx format
      if (angular.isArray(ctx)) {
        ctxAdapter = {};
        angular.forEach(ctx, function(key, value) {
          ctxAdapter[key.id] = key;
        });
      }

      return ctxAdapter;
    }

    /**
     * @ngdoc Method
     * @name adapterForOldCtx
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Convert new ctx in old ctx format
     *
     * @param {object} ctx by converted.
     *
     * @returns {array} ctx in old format.
     */
    function adapterForOldCtx(context) {
      var ctxAdapter = context;

      if (isValidObject(context)) {
        ctxAdapter = [];
        angular.forEach(context, function(key) {
          ctxAdapter.push(key);
        });
      }

      return ctxAdapter;
    }


    /**
     * @ngdoc Method
     * @name getMockDataPath
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Make the Absolute Path for mockdata file
     *
     * @param {string} mockFile json file name to be used.
     * @param {string} fileExtension file extensions
     * @returns {string} Absolute Path.
     */
    function getMockDataPath(sourceJsonFileName, extension) {
      return dcyConstants.MOCK_DATA_BASE_PATH + '/' + sourceJsonFileName.split('.').join('/') + '.' + extension;
    }

    /**
     * @ngdoc Method
     * @name getTemplatePath
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Returns the path for template
     *
     * @param {string} templateName is the name of template.
     * @returns {string} Absolute Path.
     */

    function getTemplatePath(basePath) {
    	var dcyEnvProvider = invokeService('dcyEnvProvider');    
    	return dcyEnvProvider.getItem('domainUrl') + dcyEnvProvider.getItem('contextPath') + basePath + '/index.html';
    }
    
    /**
     * @ngdoc Method
     * @name getSharedLibraryPath
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Returns the path for shared library
     *
     * @param {string} logicalName is the logical name of shared.
     * @returns {string} Absolute Path.
     */
    
    function getLibraryPath(logicalName) {
     	var dcyEnvProvider = invokeService('dcyEnvProvider');
    	return dcyEnvProvider.getItem('domainUrl') + dcyEnvProvider.getItem('contextPath') + dcyEnvProvider.getShLibTokenizedPath() + '/' + logicalName;
    }
    
    /**
     * @ngdoc Method
     * @name resolveDependencies
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Includes the dependencies by requirejs.
     * In this routine it was used loadsh apis.
     *
     * @param  {array} resources  service response data; 
     * This routine expect the array of strings of paths or array of objects of resources.
     * Formats allowed :
     * 						resources = ['path1', 'path2'] or
     * 						resources = [{hash : 'xxxx1', uri : 'path1'}, {hash : 'xxxx2', uri : 'path2'}] 
     * 
     * @param  {string} type of resources (example : 'js' || 'css')
     */
    
    function resolveDependencies(resources, basePath, successCallback, errorCallback) {

    	basePath = isValidString(basePath) 	? basePath 	: '';

    	var callSuccessCallback = function(){
	    		if(angular.isFunction(successCallback)){
					successCallback(arguments);
				}
    		},
	    	callErrorCallback = function(){
	    		if(angular.isFunction(errorCallback)){
	    			errorCallback(arguments);
	    		}
	    	},
	    	getUriResource = function(resource){
	    		return isValidObject(resource) ? resource.uri : resource;
	    	},
	    	getResourceType = function(resource){
	    		var uri = getUriResource(resource),
	    			type = uri.substr(uri.lastIndexOf('.') + 1, uri.length);
	    		
	    		return type;
	    	},
    		skyppedResources = new Array();
    		
    	if (isValidArray(resources)) {

    		var pathNames 		= [],
	    		resourcesToLoad = [],
	    		paths 			= {},
	    		shim 			= {};

    		_.forEach(resources, function(resource){
    			var type = getResourceType(resource),
					uri  =  getUriResource(resource),					
	                hash = isValidString(resource.hash) ? resource.hash : 'TR' + (hashCode(basePath + uri)),
	                hashToLoad = type  === 'css' ? 'css!' + hash : hash;
    			//Detect if hash code for resource is valid.
    			if(!isValidString(hash)){
    				$log.info('UTILS FACTORY : Resource : ', resource, ' not valid. It has been skipped.');
    				skyppedResources.push(resource);
    			}
    			//Detect if resource is already loaded; in this case skypped it.
    			else if(_.indexOf(requirejs.dcy.resourcesLoaded, hashToLoad) >= 0){
    				$log.info('UTILS FACTORY : Resource : ', resource, ' already loaded or in pending status. It has been skipped.');
    				skyppedResources.push(resource);
    			}
    			//Resource is Valid; It can be loaded.
    			else{
    				paths[hash] = (basePath.startsWith('/') ? basePath.substring(1) : basePath ) + uri.replace(new RegExp('.'+type+'$'), '');
    				shim[hash] 	= new Object();
    				shim[hash].exports = hash; 
    				pathNames.push(hash);
    				resourcesToLoad.push(hashToLoad); //Use requirejs filter for css load
    			}
    		});

    		//There are valid resources to be loaded, then configure requirejs.
    		if(isValidArray(resourcesToLoad)){
    			_.forEachRight(pathNames, function(pathName, key){
    				shim[pathName].deps = _.take(pathNames, key); 
    			});

    			requirejs.config({
    				paths: paths,
    				shim: shim
    			});

    			//Set resources as loaded
    			
    			requirejs(resourcesToLoad, function(promise){
    				requirejs.dcy.resourcesLoaded = _.union(requirejs.dcy.resourcesLoaded, resourcesToLoad);
    				
    				$log.info('UTILS FACTORY : Include Dependencies: ', resources);

    				if(promise && promise.then){ //La gestione di rilascio e' demandata alla risorsa inclusa
    					promise.then(function(){
							callSuccessCallback(skyppedResources, resourcesToLoad);
    					}, function(){
							callErrorCallback(skyppedResources);
    					});
    				}
    				else{
    					callSuccessCallback(skyppedResources, resourcesToLoad);
    				}
    				
    			}, function(){
    				$log.info('UTILS FACTORY : ERROR on Include Dependencies: ', resourcesToLoad);
					callErrorCallback(resourcesToLoad);
    			});

    			return true;
    		}
    		//Resources to be loaded not detected.
    		//If there are skypped resources call the success callback because already loaded into page. 
    		else if(skyppedResources.length){
				callSuccessCallback(skyppedResources);
				return true;
    		}
    		//Resources to be loaded not detected.
    		//There are NOT skypped resources then call error callback. 
    		else{
    			callErrorCallback(skyppedResources);
    			return false;
			}
    	}
    }

    /**
     * [isWindow description]
     * @param  {[type]}  obj [description]
     * @return {Boolean}     [description]
     */
    function isWindowElement(obj) {
      return !isUndefinedOrNull(obj) && angular.equals(obj, obj.window);
    }
    
    /**
     * [isDocument description]
     * @param  {[type]}  obj [description]
     * @return {Boolean}     [description]
     */
    function isDocumentElement(obj) {
      return !isUndefinedOrNull(obj) && angular.equals(obj, obj.document);
    }
    
    /**
     * [fetchTemplate]
     * @param  {String}  template [url or name of template]
     * @return {String}  [html of template]
     */
    function fetchTemplate(template) {
    	var $http = invokeService('$http'),
    		$q = invokeService('$q');
    	return $q.when($templateCache.get(template) || $http.get(template) ).then(function(res) {
    		if(isValidObject(res)) {
    			return res.data;
    		}
    		return res;
    	}, function(){
    		return template;
    	});
    }
    
    function getOffsetByElement(element){
    	if(!isHtmlElement(element)){
    		return false;
    	}

    	var coordinates = {
    			left 	: element.offsetLeft,
    			top  	: element.offsetTop
    	};

    	do{    	
    		coordinates.left += element.offsetLeft;
    		coordinates.top  += element.offsetTop;
    	}while ((element = element.offsetParent));

    	return coordinates;
    }
    
    /**
     * @ngdoc Method
     * @name invokeService
     * @methodOf dcyApp.UtilsFactory
     *
     * @description
     * Invoke on fly an Angular Service registered
     *
     * @param {String} Service name.
     *
     * @returns {object} instanse of service name.
     */
    function invokeService(serviceName) {
			if($injector && $injector.has(serviceName)){
				var service = $injector.get(serviceName);
				return angular.isDefined(service.$get) ? $injector.invoke(provider.$get, service) : service;
			}
			
			return null;
    }

    /**
	 * Set the first chart of string in uppercase
	 * @param {string} string
	 * @returns {string}
	 */
    function capitalizeFirstChar(string) {
        return (string.charAt(0).toUpperCase() + string.slice(1));
    }
    
    /**
	 * Check if angular component passed is registered on application
	 * @param {string} providerName : the name of provider (ex. factory/directive/service/controller)
	 * @param {string} componentName : the name of component to register 
	 * @returns {boolean} true if component is registered
	 */
    function isComponentExist(providerName, componentName){

    	switch (providerName) {	    	
	    	case 'controller':
	    		if(typeof window[componentName] === 'function') {
	    			return true;
	    		}
	    		try {
	    			var $controller = invokeService('$controller');
	    			$controller(componentName, { "$scope": {} }, true);
	    			return true;
	    		} catch (error) {
	    			return  false; 
	    		}
	
	    		break;
	
	    	case 'filter':
	    	case 'directive':
	    		return $injector.has(componentName + capitalizeFirstChar(providerName));
	    		break;
	
	    	case 'service':
	    		return $injector.has(componentName);
	    		break;
	
	    	default:
	    		return false;
	    		break;
    	};
    	
    }
    
    /**
	 * Check if module passed is registered on application
	 * @param {string} moduleName : the name of module
	 * @returns {boolean} true if module is registered
	 */
    function isModuleExist(moduleName){
		try {
			angular.module(moduleName);
		} catch(e) {
			return false;
		}
		return true;	
    }
    
     //Start Private for message management
    function getMessage(type, message, forUser, objectToFire) {
    	var messageValidation = function(message, forUser){
    		if(isValidString(message) && (window.ENV_VARS.isInDevelopmentOrTesting() || forUser)){
    			return true;
    		}
    		return false;
    	};
    	
    	if(messageValidation(message, forUser)){
    		var arrToReplaced = [type, objectToFire?objectToFire:''];
    		return window.replaceAllForPosition('#DCY %1#[%2]:', '%(\\d+)', arrToReplaced) + message;
    	}
    	return false;
    }
    //End Private for message management
    
    function infoToConsole(message, objectToFire, forUser){
		var messageToShow = getMessage('INFO', message, forUser, objectToFire);
		$log.info(messageToShow);
	}
	
	function warningToConsole(message, objectToFire, forUser){
		var messageToShow = getMessage('WARNING', message, forUser, objectToFire);
		$log.warn(messageToShow);
	}
	
	function debugToConsole(message, objectToFire, forUser){
		var messageToShow = getMessage('DEBUG', message, forUser, objectToFire);
		$log.debug(messageToShow);
	}
	
	function logToConsole(message, objectToFire, forUser){
		var messageToShow = getMessage('LOG', message, forUser, objectToFire);
		$log.log(messageToShow);
	}
	
	function errorToConsole(message, objectToFire, forUser){
		var messageToShow = getMessage('ERROR', message, forUser, objectToFire);
		$log.error(messageToShow);
	}
	
	function registerComponent(componentName, constructor, providerName, isUserComponent){
		var EXT_APP_MODULE = dcyConstants.EXT_APP_MODULE,
			registeredResult = null;
		//Module component	
		if(angular.equals(providerName, 'module')){
			registeredResult = invokeAngularService('dcyModule').registerModule(componentName);	
		}
		//Service/Directive/Factory/Provider/Controller component		
		else if(!isComponentExist(providerName, componentName)){			
			registeredResult = angular.module(EXT_APP_MODULE).register[providerName](componentName, constructor);
		}
		

		infoToConsole('The ['+componentName+'] '+(registeredResult !== null ? 'was registered' : 'is already')+' on application!', 'registerComponent', isUserComponent);

		return registeredResult;
	}
		
  function manageHandlerQueue(event){
  	if(event.defaultPrevented) {
  		return false;
  	}
  	event.preventDefault();
  	return true;
  }

  function getTemplateInstance(element){
  	if(!isHtmlElement(element)){
  		return false;
  	}
  	var $dcycontent = $(element).closest('dcycontent'),
  		nodeInfo;

  	if(!$dcycontent.length){
  		return null;
  	}

  	nodeInfo = $dcycontent.data('nodeInfo');

  	if(isValidObject(nodeInfo)){
  		return nodeInfo.templateInstance;
  	}

  	return false;
  }
  
  /**
	 * @ngdoc method
	 * @name isRightClickEvent
	 * @methodOf 
	 *
	 * @description Return if event passed is right click
	 *
	 * @param {object} $event 
	 * @returns {boolean} true if event is right click
	 */
  function isRightClickEvent($event){
	  var e = $event || window.event; //Se non mi viene passato cerco nell'event della window

	  // Cerco la propriet� 'which' nell'event (c'e' solo nel caso di evento al tasto destro) 
	  // Gestione per => Gecko(Firefox), WebKit (Safari/Chrome) e Opera
	  if("which" in e){
		  return (angular.equals(e.which, 3)); 
	  }
	  // Gestione per => IE e Opera
	  else if("button" in e){
		  return (angular.equals(e.button, 2));
	  }
	  
	  return false;
  }
  
  	/**
	 * @ngdoc method
	 * @name formatJson
	 * @methodOf 
	 *
	 * @description Format a josn string
	 *
	 * @param {string} the json to be format
	 * @returns {string} the formatted json
	 */
	function formatJson(json) {
		var formattedJson = json;
		try {
			formattedJson = angular.toJson(angular.fromJson(json), true);
		}
		catch (e) {

		} 

		return formattedJson;
	}
	
	function hashCode(str){
	    var hash = 0, i, char;
	    if (str === null || str.length == 0) return hash;
	    for (i = 0; i < str.length; i++) {
	        char = str.charCodeAt(i);
	        hash = ((hash<<5)-hash)+char;
	        hash = hash & hash; // Convert to 32bit integer
	    }
	    return hash;
	};
    
	/**
	 * @ngdoc method
	 * @name openNewWindow
	 * @methodOf 
	 *
	 * @description Open url in new browser tab
	 *
	 * @param {string} path : relative path
	 * @param {string} name : Specifies the target attribute or the name of the window
	 */
	function openPathInNewTab(path, name){
		var dcyEnvProvider = invokeService('dcyEnvProvider'),
			domainUrl = dcyEnvProvider.getItem('domainUrl', null, 'applicationInfo'),
			contextPath = dcyEnvProvider.getItem('contextPath'),
			url = domainUrl + contextPath + path;
			
		window.open(url, name);
	}
	
	/**
	 * @ngdoc method
	 * @name uuid
	 * @methodOf 
	 *
	 * @description Generate uuid
	 * 	
	 * @return {string} univocity id exadecimal based (example '00000000-0000-0000-0000-000000000000');
	 */
	function uuid(){
		var  _p8 = function(s) {
			//Il numero restituito da 'Math.random' lo converto in una stringa esadecimale
			//come ad esempio '0.6fb7687f'; poi tolgo lo 0 e la ','.
			var p = (Math.random().toString(16)+"000000000").substr(2,8);
			return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
		};
		return _p8() + _p8(true) + _p8(true) + _p8();
	};
    
	function deepMerge(obj1, obj2) {
		
		//Nel secondo caso effettuo una pulizia di 'obj1' sulla base delle properties di 'obj2';
		//Eccezioni : Nel caso di proprieta' non definita in 'obj2' e la stessa di 'obj1' risulta essere un oggetto..elimino quest'ultima da 'obj1'.
		var cleanObjects = function(obj1, obj2){
			angular.forEach(obj1, function(value, key) {
				if(angular.isObject(value)) {
					if (isValidObject(obj2[key])){
						cleanObjects(value, obj2[key]);
					}
					else if(angular.isArray(obj2[key])){
						obj1[key] = obj2[key];
					}
					else if(angular.isUndefined(obj2[key])){
						if (angular.isArray(obj1)) {
							obj1 = obj1.splice(key, 1);
						}
						else {
							delete obj1[key];							
						}
					}
				}
			});
		};
		
		angular.merge(obj1, obj2);
		cleanObjects(obj1, obj2);
		
		return obj1;
	}
	
	/**
	 * @ngdoc method
	 * @name defineMetaEvent
	 * @methodOf 
	 *
	 * @description Register new javascript event with namespace without JQuery
	 * 
	 * @param {string} namespace event
	 * @param {string} real javascript event
	 * 	
	 * @return add/remove/clear function on javascript Element prototype
	 */
	function registerMetaEvent(namespace,realEvent) {
		var eventName = 'on'+namespace+realEvent, //Costruisco il namespace dell'evento
			realEventLw = realEvent.toLowerCase(); //evento javascript reale
		//Gestisco il caso di evento gia registrato
		try {
			Object.defineProperty(Element.prototype, eventName, {get:function() {

				//ELemento su cui e' stato registrato l'evento
				var el = this,
				//Array dove andr� a storare gli handler dell'evento richiamato
				handlers = [],

				//L'interfaccia che vado ad esporre su ogni elemento x l'evento in questione
				wrapper = {
						//Serve ad aggiungere un nuova callback
						add: function(handle) {
							if(typeof handle !== 'function'){
								return;
							}
							//Aggiungo all'inizio dell'array la nuova callback
							var newLength = handlers.unshift(handle); 
							//Se � la prima callback effettuo il vero e proprio bind
							if(newLength === 1) { 
								rebind(); 
							}
						},
						//Serve a rimuovere un handle esistente
						remove: function(handle) {
							//Catturo l'indice dell'handle passato nell'array 
							var index = handlers.indexOf(handle); 
							//Se esiste
							if(index) {
								//Elimino l'handle dall'array
								handlers.splice(index, 1);
								//Se non ci sono pie' handle presenti
								//effettuo l'unbind dell'evento
								if(handlers.length==0) { 
									unbind(); 
								}
							}
						},
						//Rimuove tutti gli handlers
						clear: function() {
							handlers.length = 0; 
							unbind();
						}
				},
				//Chiamo tutti gli handlers definiti
				metaHandler = function(e) {
					for(var i=handlers.length; i--;) {
						try{ 
							handlers[i].call(el, e); 
						}catch(ex){}
					}
				},
				//Bind del metaHandler sull'evento reale
				unbind = function() {
					el.removeEventListener(realEventLw, metaHandler);
				},
				//Re-Bind del metaHandler sull'evento reale
				rebind = function() {
					el.addEventListener(realEventLw, metaHandler, false);
				};

				//Aggiungo le property da espoorre contenute nel wrapper
				Object.defineProperty(el, eventName, {value : wrapper});
				return wrapper;
			}});
		}
		catch(err) {}		  
	}
	
	function isCursorInElements(excludeListOfQueriesElements) {
		
		if(!isValidArray(excludeListOfQueriesElements) || !$window.event){
			return false;
		}
		
		var hoveredElement;
		
		_.forEach(excludeListOfQueriesElements, function(query) {
			var $element = angular.element(query);
			if($element.get(0)){
				var offset 		= $element.offset(),
					left 		= offset.left,
					top 		= offset.top,
					height 		= $element.height(),
					width 		= $element.width(),
					maxx 		= left + width,
					maxy 		= top + height;
				
				if (($window.event.pageY <= maxy && $window.event.pageY >= top) && ($window.event.pageX <= maxx && $window.event.pageX >= left)){
					hoveredElement = $element;
				};
			}
		});
		
		return hoveredElement;
	}
	
	function isJsonString(string){
	    try{
	    	if(isValidString(string)){
	    		angular.fromJson(string);
	    	}
	    	else{
	    		return false;
	    	}
	    }catch(e){
	        return false;
	    }
	    return true;
	}
	
	function getCtxValue(ctx, parameter){
		if(!isUndefinedOrNull(ctx)){
			//Vuona struttura
			if(isValidObject(ctx)){
				return ctx[parameter];
			}
			else if(isValidArray(ctx)){
				return getCtxValue(parameter, ctx);
			}
		}
		
		return null;
	}
	
	function getFormatBytes(bytes,decimals) {
		if(bytes == 0) return '0 Byte';
		var k = 1000;
		var dm = decimals + 1 || 3;
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		var i = Math.floor(Math.log(bytes) / Math.log(k));
		return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + '' + sizes[i];
	}
	
  return {
  		isValidObject : isValidObject,
		isValidArray : isValidArray,
		isValidString : isValidString,
		isHtmlElement : isHtmlElement,
		findElement : findElement,
		replaceAll : replaceAll,
		addEventListener : addEventListener,
		removeEventListener : removeEventListener,
		getPropertyStyleByElement : getPropertyStyleByElement,
		getZindexByElement : getZindexByElement,
		isUndefinedOrNull : isUndefinedOrNull,
		adapterForNewCtx : adapterForNewCtx,
		adapterForOldCtx : adapterForOldCtx,
		getMockDataPath : getMockDataPath,
		getTemplatePath : getTemplatePath,
		getLibraryPath : getLibraryPath,
		resolveDependencies : resolveDependencies,
		isWindowElement : isWindowElement,
		isDocumentElement : isDocumentElement,
		fetchTemplate : fetchTemplate,
		getOffsetByElement : getOffsetByElement,
		invokeService : invokeService,
		isComponentExist : isComponentExist,
		infoToConsole : infoToConsole,
		warningToConsole : warningToConsole,
		debugToConsole : debugToConsole,
		logToConsole : logToConsole,
		errorToConsole: errorToConsole,
		registerComponent : registerComponent,
		manageHandlerQueue : manageHandlerQueue,
		getTemplateInstance : getTemplateInstance,
		formatJson : formatJson,
		isRightClickEvent : isRightClickEvent,
		hashCode : hashCode,
		openPathInNewTab : openPathInNewTab,
		uuid : uuid,
		capitalizeFirstChar : capitalizeFirstChar,
		deepMerge : deepMerge,
		registerMetaEvent : registerMetaEvent,
		isCursorInElements : isCursorInElements,
		isJsonString : isJsonString,
		getCtxValue : getCtxValue,
		getFormatBytes : getFormatBytes,
    };
  }

  UtilsFactory.$inject = ['$injector', '$log','dcyConstants','$templateCache','$timeout', '$window'];
  angular
    .module('dcyApp.factories')
    .factory('dcyUtilsFactory', UtilsFactory);
}());
