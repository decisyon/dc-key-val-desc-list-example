(function(dcyAngular) {
  'use strict';

  /**
   * Con la funzionalit� che segue viene esteso il comportamento
   * dell'api 'angular.module' nativa a registrare tutti i moduli 
   * registrati nell'applicazione; quest'ulteriore api � raggiungibile
   * attraverso l'istruzione angular.modules_registered.
   */
  (function() {
	  var orig_angular_module = angular.module;
	  angular.modules_registered = [];
	  angular.module = function() {
		  var args = Array.prototype.slice.call(arguments);
		  //Controllo che sia effettivamente una registrazione e non un get
		  if (arguments.length > 1) {
			  angular.modules_registered.push(arguments[0]);
		  }
		  return orig_angular_module.apply(null, args);
	  };
  })();
  
  /**
   * Con la funzionalita' che segue viene esteso il comportamento
   * di requirejs in quanto pushamo nel suo module.export un oggetto
   * dcy in cui storiamo potenziali informazioni sulle risorse trattate
   * che serviranno al sistema. Infatti predisponiamo al suo interno
   * l'Array resourceLoaded in cui verranno storati gli hashCode delle
   * risorse caricate (viene fatto dal resolveDependencies su utils.factory.js)
   * 
   * PS. SI PUO' PENSARE DI CREARE UN SERVIZIO A WRAPPARE REQUIRE.JS ED ESPORRE
   * DELLE API A NOI COMODE. 
   */
  (function() {
	  if(requirejs){
		  requirejs.dcy = {
				  resourcesLoaded 	: new Array()
		  };
	  }
  })();
  
  /* Core modules */
  angular.module('dcyApp.constants', []);
  angular.module('dcyApp.providers', []);
  angular.module('dcyApp.filters', []);
  angular.module('dcyApp.services', []);
  angular.module('dcyApp.factories', []);
  angular.module('dcyApp.directives', []);
  angular.module('dcyApp.controllers', []);

  /*For user components register*/
  angular.module('dcyApp.ext.templates', []);
  
  
  /**
   * @ngdoc service
   * @name dcyApp:dcyConstants
   * @requires window
   *
   * @description
   * This module set the default information of dcyApp
   * use "dcyConstants" inside your module dependencies.
   * @example
   * ```js
   * function myFn($routeParams, dcyConstants){
   *     var my_path = dcyConstants.SRC_PATH;
   * }
   * myFn.$inject = ['$routeParams', 'dcyConstants'];
   *
   * angular.module('dcyApp.factories')
   *     .factory('myModules', myFn);
   * ```
   *
   * @return {object} Newly created cache object with the following set of methods:
   */
  function DcyConstants() {
	var Constants 		= {},
	  	rootPath 		= dcyAngular.SESSION_DATA.data.applicationInfo.rootPath,
		scrPath 		= dcyAngular.SESSION_DATA.data.applicationInfo.srcPath,
		USE_MOCK_DATA 	= dcyAngular.SESSION_DATA.data.applicationInfo.useMockData;
    
    Constants = {
      MOCK_DATA_BASE_PATH: rootPath + '/mockData',
      USE_MOCK_DATA: USE_MOCK_DATA,
      VIEWS_CONVENTION_REF: '.template',
      CONTROLLERS_CONVENTION_REF: '.controller',
      SERVICES_CONVENTION_REF: '.service',
      FACTORIES_CONVENTION_REF: '.factory',
      DIRECTIVES_CONVENTION_REF: '.directive',
      TEMPLATES_PATH : scrPath + 'modules/templates/',
      COMPONENTS_PATH : scrPath + 'modules/components/',
      DEVEL_PATH : rootPath + scrPath, 
      SECTIONS_PATH :  scrPath + 'modules/sections/',
      DCY_APP_MODULE : 'dcyApp',
      EXT_APP_MODULE : 'dcyApp.ext.templates',
      HTTP_POST_CONTENT_TYPE : 'application/x-www-form-urlencoded; charset=UTF-8',
      TEMPLATE_SDK_DOC_PATH : '/docs/widget_sdk/en/index.html#/widget_sdk/@Introduction'
    };
    return Constants;
  }

  function DeviceInfo() {
    var ua = navigator.userAgent;
    //logToConsole(ua);

    function getBrowser() {

      var browsers = {
        chrome: /\bChrome\b/,
        safari: /\Safari\b/,
        firefox: /\Firefox\b/,
        ie: /\bMSIE|Trident\b/,
        opera: /\Opera\b/
      };

      for (var key in browsers) {
        if (browsers[key].test(ua)) {
          return key;
        }
      }

      return 'unknown';

    }

    function getOS() {
      var os = {
        windows: /\bWindows\b/,
        mac: /\bMac OS\b/,
        linux: /\bLinux\b/,
        unix: /\bUNIX\b/,
        android: /\bAndroid\b/,
      };

      for (var key in os) {
        if (os[key].test(ua)) {
          return key;
        }
      }

      return 'unknown';
    }

    function getDevice() {
      var device = {
        android: /\bAndroid\b/,
        ipad: /\biPad\b/,
        iphone: /\biPhone\b/,
        blackberry: /\bblackberry\b/,
        macintosh: /\bMacintosh\b/
      };

      for (var key in device) {
        if (device[key].test(ua)) {
          return key;
        }
      }

      return 'unknow';
    }

    return {
      OS: getOS(),
      BROWSER: getBrowser(),
      DEVICE: getDevice()
    };
  }

  //Define a new module dcyApp.constants.dcyConstants
  angular.module('dcyApp.constants')
    .constant('dcyConstants', new DcyConstants())
    .constant('deviceInfo', new DeviceInfo())
    .constant('dcyTranslateProvider', {});

}(window.dcyAngular));
