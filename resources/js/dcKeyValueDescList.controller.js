(function () {
    'use strict';
    
    function DCKeyValueDescController($scope, $timeout, $q) {
        $scope.DECISYON.target.registerDataConnector(function (requestor) {
            var deferer = $q.defer();
            
            $timeout(function(){
                
                deferer.resolve([
								{
									'key' : 'PARAM_NOT_FOUND',
									'val' : 'No Selection'
								},
								{
									'key' : '1',
									'val' : 'iPhone'
								},
								{
									'key' : '2',
									'val' : 'Android'
								},
								{
									'key' : '3',
									'val' : 'Blackberry'
								},
								{
									'key' : '4',
									'val' : 'Windows Phone'
								}
							]);
            },10);
            
            return {
                data : deferer.promise
            };
        });
        
    }
    
    DCKeyValueDescController.$inject = ['$scope', '$timeout', '$q'];
    DECISYON.ng.register.controller('dcKeyValueDescCtrl', DCKeyValueDescController);
    
}());