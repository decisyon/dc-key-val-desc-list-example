(function () {
    'use strict';
    
    function DCKeyValueDescController($scope, $timeout, $q) {
        $scope.DECISYON.target.registerDataConnector(function (requestor) {
            var deferer = $q.defer();
            
            $timeout(function(){
				
				var defaultEntries = [
					{
						'key' : '1',
						'val' : 'iPhone'
					},
					{
						'key' : '2',
						'val' : 'Android'
					},
					{
						'key' : '3',
						'val' : 'Blackberry'
					},
					{
						'key' : '4',
						'val' : 'Windows Phone'
					}
				];
				
                deferer.resolve(defaultEntries);
							
            },10);
            
            return {
                data : deferer.promise
            };
        });
        
    }
    
    DCKeyValueDescController.$inject = ['$scope', '$timeout', '$q'];
    DECISYON.ng.register.controller('dcKeyValueDescCtrl', DCKeyValueDescController);
    
}());